package fr.cd.jtetris.util;
 
 /**
  * Utilitaire de gestion des blocs
  * 
  * @author CD
  * @version 1.0.0
  * @since 1.0.0
  */
  public final class AutreUtil {
        
	        /**
		 * Constructeur privé pour classe utilitaire
                 */
	         private AutreUtil() {
					          }
						   
	        /**
		 * Renvoie la colonne
		 * 
		 * 
		 * @param parts Un bloc
		 * @param index Index de la colonne recherchée
		 * @return Valeur de la colonne
		 * @since 1.0.0
		 */
																           public static int getAutreColonne(int[] parts, int index, int largeur) {
																	                   final int hauteur = parts.length;
												    
																		                    int val = 0;
											     
		                          for (int i=0; i<hauteur; i++) {
		                           int part = parts[i];
		                          val += (part & 1 << (largeur - index - 1)) == 0 ? 0 : 1 << (hauteur - 1 - i); 
		                          }
																	                      return val;
																		              }
																		       }
